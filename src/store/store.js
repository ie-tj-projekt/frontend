import { configureStore } from '@reduxjs/toolkit'
import {userSlice} from './userSlice'
import {groupSlice} from './groupSlice'
import { getDefaultMiddleware } from '@reduxjs/toolkit';

const customizedMiddleware = getDefaultMiddleware({
  serializableCheck: false
})

export default configureStore({
  reducer: {
    user: userSlice.reducer,
    group: groupSlice.reducer,

  },
})