import React, {useState, useEffect} from 'react'
import {useDispatch, useSelector} from 'react-redux'
import { Container,Navbar,Nav,Button, } from 'react-bootstrap'
import { userSelector, clearState } from '../store/userSlice'
import { useHistory } from 'react-router-dom';



export default function HomeNavbar() {

    const dispatch = useDispatch()
    const { isFetching, isSuccess, isError, errorMessage } = useSelector(userSelector)

   

    const logoutHandler = e => {
       dispatch(clearState());
    }

    return (
        <Container fluid >
            <Navbar  bg="secondary" >
                <Navbar.Brand href="#home">Crypto Diary</Navbar.Brand>
                <Nav className="mr-auto">
                </Nav>
              {(isSuccess) ? (<Button variant="success" onClick={logoutHandler}>Wyloguj</Button>): null}
            </Navbar>
        </Container>
    )
}

