import React, {useState, useEffect} from 'react'
import styles from '../styles/styles.module.css'
import {Button, Form, Col, Alert} from 'react-bootstrap'
import { useSelector, useDispatch } from 'react-redux';
import { signupUser, userSelector, clearState } from '../store/userSlice';
import Loader from "react-loader-spinner";



export default function RegisterTile() {

    const dispatch = useDispatch()
    const {isRegisterSuccess, isFetching, isError} = useSelector(userSelector)

    const [firstName, setFirstName] = useState('')
    const [lastName, setLastName] = useState('')
    const [email, setEmail] = useState('')
    const [password, setPassword] = useState('')

    const submitHandler = (e) =>{
        e.preventDefault()
        const newUser = {
            firstName: firstName,
            lastName: lastName,
            email: email,
            password: password
        }

       
        dispatch(signupUser({firstName,lastName,email,password}))
        dispatch(clearState)
    }

    

    return (
        <div className={styles.registerTile}>
            
            <h3 className={styles.userTileTitle}>Rejestracja</h3>
            <Form 
                onSubmit={submitHandler}
                style={{
                    display:"flex",
                    flexDirection:"column",
                    justifyContent:"center",
                    alignItems:"center"
                }}>
                <Form.Row>
                    <Form.Group as={Col} controlId="formFirstName">
                        <Form.Label>Imię</Form.Label>
                        <Form.Control
                            placeholder="Wprowadź imię"
                            value={firstName}
                            onChange={(e)=>setFirstName(e.target.value)} />
                    </Form.Group>

                    <Form.Group as={Col} controlId="formLastName">
                        <Form.Label>Nazwisko</Form.Label>
                        <Form.Control 
                            placeholder="Wprowadź nazwisko"
                            value={lastName}
                            onChange={(e)=>setLastName(e.target.value)} />
                    </Form.Group>
                </Form.Row>
                <Form.Group as={Col} controlId="formEmail">
                    <Form.Label>Email</Form.Label>
                    <Form.Control 
                        type="email"
                        placeholder="Wprowadź adres email"
                        value={email}
                        onChange={(e)=>setEmail(e.target.value)} />
                </Form.Group>

                <Form.Group as={Col} controlId="formGridEmail">
                    <Form.Label>Hasło</Form.Label>
                    <Form.Control 
                        type="password"
                        placeholder="Wprowadź hasło"
                        value={password}
                        onChange={(e)=>setPassword(e.target.value)} />
                </Form.Group>

                <Button variant="success" type="submit" 
                style={{
                    width:"200px"
                }}>
                Zarejestruj
                </Button>
                {isFetching ? <Loader
                    type="Rings"
                    height={50}
                    width={50}
                /> : isRegisterSuccess ? (
                    <Alert style={{marginTop:"5%"}} variant="success" >Utworzono użytkownika</Alert>
                ): isError ?(
                    <Alert style={{marginTop:"5%"}} variant="danger" >Błąd przy tworzeniu użytkownika</Alert>
                ): null}

            </Form>

        </div>
    )
}
