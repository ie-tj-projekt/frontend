import React, {useEffect} from 'react'
import styles from '../styles/styles.module.css'
import LoginTile from '../components/LoginTile'
import RegisterTile from '../components/RegisterTile'
import LoginBanner from '../components/LoginBanner'
import { Container,Row, Col } from 'react-bootstrap'
import { useDispatch, useSelector } from 'react-redux'


export default function LoginScreen({history}) {

    return (
        <Container >
            <Row>
                <LoginBanner name={'Crypto Diary'} />
            </Row>
            <Row>
                <Col>
                    <LoginTile />
                </Col>
                <Col>
                    <RegisterTile />
                </Col>
            </Row>
            
        </Container>

    )
}
