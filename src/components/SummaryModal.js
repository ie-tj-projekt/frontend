import React, {useState,useEffect} from 'react'
import {useSelector,useDispatch} from 'react-redux'
import {userSelector,generateReport} from '../store/userSlice'
import { Button,Modal,Form,Alert,Table } from 'react-bootstrap'
import styles from '../styles/styles.module.css'


export default function SummaryModal(props) {

    const dispatch = useDispatch()
    const { groups,activeGroup,isLogged,isFetching, errorMessage } = useSelector(userSelector)

    const [groupList, setGroupList] = useState(groups)

    useEffect(() => {
       if(isLogged){
           if(groups.length > 0){
               const temp = groups.map(item => ({...item,checked:false}))
               setGroupList(temp)
           }
       }
    }, [groups])

   
    const changeHandler = (event) => {
        const index = event.target.id
        let arr = [...groupList]
        let item = { ...groupList[index] }
        item.checked = !item.checked
        arr[index] = item
        setGroupList(arr)
    }

    const submitHanler = (e) => {
        e.preventDefault()
        if(groupList.length <= 0) return
        const selectedGroups = groupList.filter(item => item.checked == true)

        let list = []
        selectedGroups.map(item => list.push(item.name))


        dispatch(generateReport(list))
        props.handleClose()

    }

    return (
        <div>
            <Modal show={props.show} onHide={props.handleClose}>
        <Form onSubmit={submitHanler}>
          <Modal.Header >
            <Modal.Title>Wprowadź dane do podsumowania</Modal.Title>
          </Modal.Header>
          <Modal.Body style={{display:"flex",justifyContent:"center", alignItems:"center"}}>
            {isFetching ? (<h2>Loading</h2>) : (groupList.length > 0) ? (
                <Table style={{width:"60%"}} striped bordered hover size="md">
                    <thead>
                        <tr align="center" >
                            <th >Nazwa grupy</th>
                            <th >Wybór</th>
                        </tr>
                    </thead>
                    <tbody>
                        {groupList.map((item,index)=>(
                            <tr align="center">
                                <td >{item.name}</td>
                                <td>
                                <Form.Check 
                                checked={item.checked} 
                                value={item.name} 
                                onClick={changeHandler} 
                                id={`${index}`} 
                                type="radio"
                                /> 
                                </td>
                            </tr>
                        ))}
                    </tbody>
                </Table>
            ) : (<Alert variant="primary">Użytkownik nie posiada grup</Alert>)}
    
          </Modal.Body>
          <Modal.Footer>
          <Button type="submit" variant="success" >
              Generuj
            </Button>
            <Button onClick={props.handleClose} variant="danger" >Zamknij</Button>
          </Modal.Footer>
        </Form>
      </Modal>
        </div>
    )
}
