import { createSlice, createAsyncThunk } from '@reduxjs/toolkit'
import data from './data.json'
export const addGroup = createAsyncThunk(
    'user/add_group',
    async(group, thunkApi) =>{
        try {
            console.log("try to add new group: ", group);
            const {name,transactions} = group

            const response = await fetch(
                '/user/add_group',
                {
                    method: 'POST',
                    headers: {
                        Accept: 'application/json, text/plain, */*',
                        'Content-Type': 'application/json',
                        Authorization: `Bearer ${localStorage.getItem('token')}`
                    },
                    body: JSON.stringify({
                        name,
                        transactions
                    })
                }
            )
            
            console.log("Reponse: ", response);
            let data = await response.json()
            console.log("Data here: " + data);

            if(response.status === 200){
                console.log("add group OK");
                return{...data}
            }
            else{
                return thunkApi.rejectWithValue(response)
            }
        } catch (error) {
            console.log('Error: ', error.response.data);
      return thunkApi.rejectWithValue(error.response.data)
        }
    }
)

export const getGroup = createAsyncThunk(
    'group/getGroup',
    async(group,thunkApi) =>{
        try {
            console.log("try to get group");
            const response = data
            console.log(response);
            return response
        } catch (error) {
            console.log("Error");
            thunkApi.rejectWithValue(error.response.data)
        }
    }
)

export const updateGroup = createAsyncThunk(
    'user/update_group',
    async(thunkApi) => {
        try {
            console.log("try to update group");
        } catch (error) {
            console.log("Error");
            thunkApi.rejectWithValue(error.response.data)
        }
    }
)

export const groupSlice = createSlice({
    name: 'group',
    initialState:{
        group: [],
        isFetching: false,
        isSuccess: false,
        isError: false,
        errorMsg: '',
    },

    reducers: {
        clearState: (state) => {
          state.isError = false;
          state.isSuccess = false;
          state.isFetching = false;
    
          return state;
       
        },
      },

    extraReducers: {

        [getGroup.fulfilled]: (state,{payload})=>{
            state.isFetching = false
            state.isSuccess = true
            state.group = payload.group
        },
        [getGroup.pending]: (state)=>{
            state.isFetching = true
        },
        [getGroup.rejected]: (state,{payload})=>{
            state.isFetching = false
            state.isError = true
            state.errorMsg = payload
        },

        [addGroup.fulfilled]: (state,{payload})=>{
            state.isFetching = false
            state.isSuccess = true
            state.group = [...state.group, payload]
        },

        [addGroup.pending]: (state)=>{
            state.isFetching = true
        },

        [addGroup.rejected]: (state,{payload})=>{
            state.isFetching = false
            state.isError = true
            state.errorMsg = "Błąd przy dodawaniu grupy"
        },
        [updateGroup.fulfilled]: (state,{payload})=>{
            state.isFetching = false
            state.isSuccess = true
            
        },

        [updateGroup.pending]: (state)=>{
            state.isFetching = true
        
        },
        [updateGroup.failed]: (state,{payload})=>{
            state.isFetching = false
            state.isError = true
            state.errorMsg = payload
            
        },
        
    },


})

export const {groupSelector} = (state) => state.group
export const { clearState } = groupSlice.actions;

