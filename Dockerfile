FROM node:12.4.0-alpine

WORKDIR /app

COPY package.json ./
COPY package-lock.json ./

RUN npm install --silent

COPY src/ src/
COPY public/ public

ENV PORT=3000
CMD ["npm","start"]

