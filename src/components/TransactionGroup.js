import React, {useEffect, useState} from 'react'
import {useDispatch,useSelector} from 'react-redux'
import styles from '../styles/styles.module.css'
import {Button, ListGroup, ListItem, Alert} from 'react-bootstrap'
import GroupModal from '../components/GroupModal'
import {setActiveGroup, userSelector} from '../store/userSlice'
import dayjs from 'dayjs'





function TransactionGroup(props) {

    const { groups,activeGroup,isLogged,isFetching, errorMessage } = useSelector(userSelector)

    const [show, setShow] = useState(false);
    const [groupList,setGroupList] = useState(groups)
    const handleClose = () => setShow(false);
    const handleShow = () => setShow(true);

    useEffect(() => {
       if(isLogged){
           if(groups.length > 0){
               setGroupList(groups)
           }
       }
    }, [groups])

    const dispatch = useDispatch()

    const clickHandler = (selectedIndex)=>{
       
        const active = groupList.find((item,index)=> index === selectedIndex)
        console.log("index: ", selectedIndex);
        console.log(active);

        dispatch(setActiveGroup(active))
    }


    return (
        <div className={styles.transactionGroupContainer}>
            <GroupModal 
             handleClose={handleClose}
             show={show}/>
            <div className={styles.transactionGroupContent}>
                <h2>Grupy transakcji</h2>
                <div className={styles.transactionListGroup}>
                    {isFetching ? (<h2>Loading</h2>) : groups.length >0 ?  (<ListGroup as="ul" >
                        {groupList.map((item, index)=>(
                            <ListGroup.Item key={index} index={index} onClick={()=>clickHandler(index)} >
                                <h6>{item.name}</h6>
                                <p>{dayjs(item.creationDate).format('DD/MM/YYYY HH:mm:ss')}</p>
                            </ListGroup.Item>
                        ))}
                    </ListGroup>) : (<Alert variant="primary">Użtkownik nie posiada grup</Alert>) }
                </div>
                <Button onClick={handleShow}>Dodaj grupę</Button>
            </div>
        </div>
    )
}

export default TransactionGroup
