import React, {useEffect} from 'react'
import {Modal, Button, Form} from 'react-bootstrap'
import { useForm } from "react-hook-form";
import {useDispatch,useSelector} from 'react-redux'
import {addTransaction, setActiveGroup} from '../store/userSlice'
import { userSelector } from '../store/userSlice'





function TransactionModal(props) {

  const dispatch = useDispatch()

 const { register, handleSubmit } = useForm();
 const { groups, isLogged } = useSelector(userSelector)

 useEffect(() => {

    if(isLogged){
      if(groups != null && props.group != null){
        const active = groups.find((item)=> item.name === props.group.name )
        dispatch(setActiveGroup(active))
      }
    }

 }, [groups])


 const onSubmit = (data,e)=>{
   console.log(data);
   e.preventDefault()
   let d = new Date();
   let newDate = d.getDate() + "." + d.getMonth() + "." + d.getFullYear()

   const newTransaction = {
     groupName: props.group.name,
     transaction: {
      //date: newDate,
      amount: data.amount,
      price: data.price,
      symbol: data.name,
      type: data.type,
      value: data.amount * data.price
     }
   }

   console.log("New Transaction: ", newTransaction);
   

   dispatch(addTransaction(newTransaction))
   props.handleClose();
 }


    return (
        <Modal show={props.show} onHide={props.handleClose}>
        <Form onSubmit={handleSubmit(onSubmit)}>
          <Modal.Header >
            <Modal.Title>Uzupełnij dane transakcji</Modal.Title>
          </Modal.Header>
          <Modal.Body>
          <p>Nie można dodać transakcji sprzedaży jeżli poprzednio waluta nie została przez nas zakupiona</p>
            <Form.Label>Rodzaj transakcji</Form.Label>
            <Form.Control as="select" required {...register("type")}>
              <option value="BUY">Kupno</option>
              <option value="SELL">Sprzedaż</option>
            </Form.Control>
            <Form.Group controlId="exampleForm.ControlInput1">
              <Form.Label>Nazwa kryptowaluty</Form.Label>
              <Form.Control  placeholder="Bitcoin" required {...register("name")}/>
            </Form.Group>
            <Form.Group controlId="exampleForm.ControlInput1">
              <Form.Label>Ilość zakupionej waluty</Form.Label>
              <Form.Control type="number" step="any"  placeholder="2.21" required {...register("amount")}/>
            </Form.Group>
            <Form.Group controlId="exampleForm.ControlInput1">
              <Form.Label>Aktualna cena waluty</Form.Label>
              <Form.Control type="number" step="any" placeholder="5.123" required {...register("price")}/>
            </Form.Group>
    
          </Modal.Body>
          <Modal.Footer>
          <Button type="submit" variant="success" >
              Dodaj
            </Button>
            <Button onClick={props.handleClose} variant="danger" >Zamknij</Button>
          </Modal.Footer>
        </Form>
      </Modal>
    )
}

export default TransactionModal
