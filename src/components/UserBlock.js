import React, {useState} from 'react'
import styles from '../styles/styles.module.css'
import { userSelector, clearState } from '../store/userSlice'
import { useHistory } from 'react-router-dom';
import {useDispatch, useSelector} from 'react-redux'
import { Button, } from 'react-bootstrap'
import SummaryModal from '../components/SummaryModal'
import StatsBlock from './StatsBlock';

function UserBlock() {

    const dispatch = useDispatch()
    const { groups, firstName, lastName,isFetching, isSuccess, isError,isLogged, errorMessage } = useSelector(userSelector)

    const [show, setShow] = useState(false);
    const handleClose = () => setShow(false);
    const handleShow = () => setShow(true);

    const logoutHandler = e => {
        dispatch(clearState());
     }


    return (
        <div className={styles.userBlockContainer}>
           <div className={styles.userBlockContent}>
                <div  className={styles.userCredsContainer}>
                    <h2>{firstName}</h2>
                    <h2>{lastName}</h2>
                </div>
                
                <div style={{width:"80%"}}>
                    <Button variant="success" style={{width:"100%"}} onClick={handleShow}>Wygeneruj raport</Button>
                    {(isLogged) ? (<Button variant="success" style={{marginTop:"4%", width:"100%"}} onClick={logoutHandler}>Wyloguj</Button>): null}
                </div>
                <StatsBlock />
           </div>
        <SummaryModal   
            handleClose={handleClose}
            show={show}
        />
        </div>
    )
}

export default UserBlock
