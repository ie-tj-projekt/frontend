import { createSlice, createAsyncThunk } from '@reduxjs/toolkit'
import axios from 'axios'

export const signupUser = createAsyncThunk(
  '/register',
  async ({firstName,lastName,email,password}, thunkApi) => {
    try {
      
      
      const response = await fetch(
        'http://localhost:8080/register',
        {
          method: 'POST',
          headers: {
            Accept: 'application/json',
            'Content-Type': 'application/json',
          },
          body: JSON.stringify({
            firstName,
            lastName,
            email,
            password,
          }),
        }
      );
      
      let data = await response.json();

      console.log("Data here: " + data);
        
      //console.log("response: " + response);
      //console.log("response-status: " + response.status);

      

      if(response.status === 200){
        console.log("I am in OK status");  
        localStorage.setItem('token', data.token)
        return{...data, firstName: firstName, lastName: lastName, email: email}

      }else{
        return thunkApi.rejectWithValue(response)
      }

      

      console.log("hello new user ", firstName);

    } catch (error) {
      console.log('Error: ', error.response.data);
      return thunkApi.rejectWithValue(error.response.data)
    }
  }
)

export const loginUser = createAsyncThunk(

  '/login',
  async({email, password}, thunkApi) => {
    try {
      console.log("try to log in");
      const response = await fetch(
        '/login',
        {
          method: 'POST',
          headers: {
            Accept: 'application/json',
            'Content-Type': 'application/json',
          },
          body: JSON.stringify({
            email,
            password,
          }),
        }
      );
     

      let data = await response.json();
      console.log("Data: ", data);

      if(response.status === 200){
        console.log("Login status 200");
        localStorage.setItem('token', data.accessToken)
        return data
      }
      else{
        return thunkApi.rejectWithValue(data)
      }

    } catch (error) {
      console.log("Error");
      thunkApi.rejectWithValue(error.response.data)
    }
  }
)

export const addGroup = createAsyncThunk(
  'user/add_group',
  async(group, thunkApi) =>{
      try {
          console.log("try to add new group: ", group);
          const {name,transactions} = group

          const response = await fetch(
              '/user/add_group',
              {
                  method: 'POST',
                  headers: {
                      Accept: 'application/json, text/plain, */*',
                      'Content-Type': 'application/json',
                      Authorization: `Bearer ${localStorage.getItem('token')}`
                  },
                  body: JSON.stringify({
                      name,
                      transactions
                  })
              }
          )
          
          console.log("Reponse: ", response);
          let data = await response.json()
          console.log("Data here: " + data);

          if(response.status === 200){
              console.log("add group OK");
              return{...data}
          }
          else{
              return thunkApi.rejectWithValue(response)
          }
      } catch (error) {
          console.log('Error: ', error.response.data);
    return thunkApi.rejectWithValue(error.response.data)
      }
  }
)

export const addTransaction = createAsyncThunk(
  'user/add_transaction',
  async(newTransaction, thunkApi) =>{
      try {
          console.log("try to add new transaction: ")
          const {groupName,transaction} = newTransaction

          console.log("Group name:", groupName);
          console.log("Group transaction:", transaction);

          const response = await fetch(
              '/user/add_transaction',
              {
                  method: 'POST',
                  headers: {
                      Accept: 'application/json, text/plain, */*',
                      'Content-Type': 'application/json',
                      Authorization: `Bearer ${localStorage.getItem('token')}`
                  },
                  body: JSON.stringify({
                      groupName,
                      transaction
                  })
              }
          )
          
          console.log("Reponse: ", response);
          let data = await response.json()
          console.log("Data here: " + data);

          if(response.status === 200){
              console.log("add transaction OK");
              return{...data}
          }
          else{
              return thunkApi.rejectWithValue(response)
          }
      } catch (error) {
          console.log('Error: ', error.response.data);
    return thunkApi.rejectWithValue(error.response.data)
      }
  }
)

export const deleteTransaction = createAsyncThunk(
  '/user/delete_transaction',
  async(data, thunkApi)=>{

    try {

      const config = {
        headers: {
          Accept: 'application/json, text/plain, */*',
          'Content-Type': 'application/json',
          Authorization: `Bearer ${localStorage.getItem('token')}`
        },
        data: {
          groupName: data.name,
          transaction: {
            date: data.date
          }
        }
      }

      const res = await axios.delete('/user/delete_transaction',config)
      .then((response) => response.data.groups)

      return res

    console.log("Delete res: " + JSON.stringify(res));
      
    } catch (error) {
      return thunkApi.rejectWithValue(error.response.data)

    }
  }
)

export const deleteGroup = createAsyncThunk(
  '/user/delete_group',
  async(name, thunkApi)=>{
    try {

      const response = await fetch(
        '/user/delete_group',
        {
            method: 'DELETE',
            headers: {
                Accept: 'application/json, text/plain, */*',
                'Content-Type': 'application/json',
                Authorization: `Bearer ${localStorage.getItem('token')}`
            },
            body: JSON.stringify({
                name: name.name
            })
        }
    )

    if(response.status === 200){
      console.log("delete group OK");
      return response
    }
    else{
      return thunkApi.rejectWithValue(response)
    }
      
    } catch (error) {
      return thunkApi.rejectWithValue(error.response.data)

    }
  }
)

export const setActiveGroup = createAsyncThunk(
  'user/set_active_group',
  async(group, thunkApi) => {
    try {
      return group
    } catch (error) {
      console.log('Error: ', error.response.data);
      return thunkApi.rejectWithValue(error.response.data)
    }
  }
)


export const generateReport = createAsyncThunk(
  '/user/report',
  async(list, thunkApi)=>{
    try {

      console.log("GENERATING REPORT");
      let baseUrl = 'http://localhost:8080/user/report'
      let url = new URL(baseUrl)
      let params = url.searchParams
      list.map(item => {
        params.append('body',item)
      })
      console.log("MY url: " + url.toString());

      const response = await fetch(
        url,
        {
            method: 'GET',
            headers: {
                Accept: 'application/json, text/plain, */*',
                Authorization: `Bearer ${localStorage.getItem('token')}`
            }
        })
        .then(response => response.blob())
        .then((blob) => {
          const url = window.URL.createObjectURL(new Blob([blob],{
            type: "application/pdf"
          }));
          window.open(url);
        })



    if(response.status === 200){
      console.log("generate report OK");

      return response
    }
    else{
      console.log("LOLO");
      return thunkApi.rejectWithValue(response)
    }
      
    } catch (error) {
      console.log("ERROR");
      console.log(error);
      return thunkApi.rejectWithValue(error.response.data)

    }
  }
)

export const getStats = createAsyncThunk(
  '/user/statistics',
  async(thunkApi) => {
    try {
      
      const config = {
        headers: {
          Accept: 'application/json, text/plain, */*',
          Authorization: `Bearer ${localStorage.getItem('token')}`
      }
      }
  

      const {data} = await axios.get('/user/statistics',config)
      console.log("stats" + data);
      return data
      
    } catch (error) {
      return thunkApi.rejectWithValue(error.response.data)

    }
  }
)



export const userSlice = createSlice({
  name: 'user',
  initialState: {
      firstName: '',
      lastName: '',
      email: '',
      groups: [],
      activeGroup:'',
      stats: '',
      roles: [],
      isFetching: false,
      isSuccess: false,
      isError: false,
      isLogged: false,
      isRegisterSuccess: '',
      errorMsg: '',
      generateRaportSuccess:false
  },

  reducers: {
    clearState: (state) => {
      state.isError = false;
      state.isSuccess = false;
      state.isFetching = false;
      state.isLogged = false;
      state.generateRaportSuccess = false;
      state.firstName= '';
      state.lastName= '';
      state.email='';
      state.groups= [];
      state.activeGroup='';
      state.roles= [];
      state.isRegisterSuccess =''

      return state;
   
    },
  },

  extraReducers: {
    [signupUser.fulfilled]: (state, {payload}) => {
      state.isFetching = false
      state.isRegisterSuccess = true
     
    
    },
    [signupUser.pending]: (state) => {
      state.isFetching = true
    },
    [signupUser.rejected]: (state,{payload}) => {
      state.isFetching = false
      state.isError = true
      state.errorMsg = payload
    },

    [loginUser.fulfilled]: (state, {payload}) => {
      state.email = payload.email;
      state.firstName = payload.firstName;
      state.lastName = payload.lastName;
      state.groups = payload.groups
      state.roles = payload.roles
      state.isFetching = false;
      state.isSuccess = true;
      state.isLogged = true;
      return state;
    },
    [loginUser.pending]: (state) => {
      state.isFetching = true
    },
    [loginUser.rejected]: (state, {payload}) => {
      state.isFetching = false;
      state.isError = true;
      state.errorMessage = "Błąd przy logowaniu";
      console.log('payload: ', payload);
    },

    [addGroup.fulfilled]: (state,{payload})=>{
      state.isFetching = false
      state.isSuccess = true
      state.groups = payload.groups
      console.log('payload add: ', payload);

  },

  [addGroup.pending]: (state)=>{
      state.isFetching = true
  },

  [addGroup.rejected]: (state,{payload})=>{
      state.isFetching = false
      state.isError = true
      state.errorMsg = "Błąd przy dodawaniu grupy"
  },

[deleteGroup.fulfilled]: (state,payload)=>{
    state.isFetching = false;
    state.isSuccess = true;
    state.groups = state.groups.filter( item => item.name !== payload.meta.arg.name);
    state.activeGroup = ''
    console.log('payload delete: ', payload.meta.arg.name);

},

[deleteGroup.pending]: (state)=>{
    state.isFetching = true
},

[deleteGroup.rejected]: (state)=>{
    state.isFetching = false
    state.isError = true
    state.errorMsg = "Błąd przy usuwaniu grupy"
},

  [addTransaction.fulfilled]: (state,{payload})=>{
    state.isFetching = false
    state.isSuccess = true
    state.groups = payload.groups
},

[addTransaction.pending]: (state)=>{
    state.isFetching = true
},

[addTransaction.rejected]: (state,{payload})=>{
    state.isFetching = false
    state.isError = true
    state.errorMsg = "Błąd przy dodawaniu transakcji"
},
[deleteTransaction.fulfilled]: (state,{payload})=>{
  state.isFetching = false
  state.isSuccess = true
  state.groups = payload
  state.activeGroup = payload
  console.log("Payload: " + JSON.stringify(payload));
},

[deleteTransaction.pending]: (state)=>{
  state.isFetching = true
},

[deleteTransaction.rejected]: (state)=>{
  state.isFetching = false
  state.isError = true
  state.errorMsg = "Błąd przy usuwaniu transakcji"
},

[setActiveGroup.fulfilled]: (state,{payload})=>{
  state.isFetching = false
  state.isSuccess = true
  state.activeGroup = payload === '' ? '' : payload
},

[setActiveGroup.pending]: (state)=>{
  state.isFetching = true
},

[setActiveGroup.rejected]: (state,{payload})=>{
  state.isFetching = false
  state.isError = true
  state.errorMsg = "Błąd przy dodawaniu grupy"
},

[generateReport.fulfilled]: (state)=>{
  state.isFetching = false
  state.generateRaportSuccess = true
},

[generateReport.pending]: (state)=>{
  state.isFetching = true
},

[generateReport.rejected]: (state,{payload})=>{
  state.isFetching = false
  state.isError = true
  state.errorMsg = 'Błąd przy generowaniu raportu'
},

[getStats.fulfilled]: (state,{payload}) => {
  state.isFetching = false
  state.stats = payload
},

[getStats.pending]: (state) => {
  state.isFetching = true
},

[getStats.rejected]: (state) => {
  state.isFetching = false
  state.isError = true
  state.errorMsg = "Błąd przy pobieraniu statystyk"
}


  },
})

export const userSelector = (state) => state.user
export const { clearState } = userSlice.actions;



