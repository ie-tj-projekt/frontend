import { useSelector, useDispatch } from 'react-redux';
import React, {useState, useEffect} from 'react'
import {getStats} from '../store/userSlice'
import { userSelector, clearState } from '../store/userSlice'


function StatsBlock() {

    const dispatch = useDispatch()
    const { groups, stats,isFetching, isSuccess, isError,isLogged, errorMessage } = useSelector(userSelector)


    useEffect(() => {
       dispatch(getStats())
    }, [groups])

    return (
        <div style={{marginTop:"15%"}}>
            <h4>Podsumowanie</h4>
            <div>
                <h6>Liczba grup: {stats.numberOfGroups}</h6>
                <h6>Liczba transakcji: {stats.numberOfTransactions}</h6>
                <h6>Liczba zakupów: {stats.numberOfBuy}</h6>
                <h6>Liczba sprzedaży: {stats.numberOfSell}</h6>
                <h6>Suma zakupów: {stats.moneyInvested} $</h6>
                <h6>Suma sprzedaży: {stats.moneyCashed} $</h6>
            </div>
        </div>
    )
}

export default StatsBlock
