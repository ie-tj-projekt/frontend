import React from 'react'
import {
  BrowserRouter as Router,
  Route,
  Redirect,
  Switch,
} from 'react-router-dom'

import { Container } from 'react-bootstrap'


import HomeScreen from './screens/LoginScreen'
import UserScreen from './screens/UserScreen'
import UserWallet from './screens/UserWallet'
import Footer from './components/Footer'
import HomeNavbar from './components/HomeNavbar'

function App() {
  return (
    <>
    <main>
      <Container fluid >
        <Router>
          <Switch>
            <Route path='/login' component={HomeScreen} exact/>
            <Route path='/' component={UserScreen} exact/>
            <Route path='/wallet' component={UserWallet} exact />
    
          </Switch>
        </Router>
      </Container>
    </main>
    <footer>
      <Footer text={'Made by Jan Potocki && Marcin Zagórski'} />
    </footer>
    </>
  )
}

export default App;
