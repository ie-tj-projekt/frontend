import React, { useState } from 'react'
import {useDispatch} from 'react-redux'
import { useForm } from "react-hook-form";
import {Modal, Button, Form, Table, Alert, Col} from 'react-bootstrap'
import {addGroup} from '../store/userSlice'

function GroupModal(props) {

    const { register, handleSubmit } = useForm();
    const {register: mainRegister, handleSubmit: mainHandleSubmit} = useForm()
    const [transactionList,setTransactionList] = useState([])

    const dispatch = useDispatch()


    const deleteTransaction = (deletedIndex)=>{
      console.log("delete: " + deletedIndex);
      console.log(transactionList);
      setTransactionList(transactionList.filter((item,index) => index !== deletedIndex))
    }

    const onSubmitTransaction = (data,e)=>{
      //console.log(data);
      e.preventDefault()

      let d = new Date();
      //let newDate = d.getFullYear() + "-" + d.getMonth() + "-" + d.getDay()

      setTransactionList(transactionList => [...transactionList,{
        //date: Date(),
        amount: data.amount,
        price: data.price,
        symbol: data.name,
        type: data.type,
        value: 10,
      }])
      console.log(transactionList);
    }

    const onSubmitGroup = (data,e) => {
      console.log("ESSA");

      e.preventDefault()
      props.handleClose()

      const newGroup = {
        name: data.name,
        transactions: transactionList,
      }

      //console.log("New group" + newGroup.stringfy());
      console.log("Token: " + localStorage.getItem('token'));
      setTransactionList([])
      dispatch(addGroup(newGroup))
    }
   

    return (
        <Modal show={props.show} onHide={props.handleClose}>
     
          <Modal.Header >
            <Modal.Title>Uzupełnij dane grupy</Modal.Title>
          </Modal.Header>
          <Modal.Body>
          <p>Aby poprawnie stworzyć grupę, jako pierwszą należy dodać transakcję kupna oraz nie można dodać transakcji sprzedaży jeżli poprzednio waluta nie została przez nas zakupiona</p>
          <Form onSubmit={mainHandleSubmit(onSubmitGroup)}>
          <Form.Row className="align-items-center">
            <Col>
              <Form.Control  placeholder="Nazwa grupy" className="mb-2" required {...mainRegister("name")}/>
            </Col>
            <Col>
              <Button type="submit"  variant="success" className="mb-2">
                Dodaj grupę
              </Button>
            </Col>
            </Form.Row>
          </Form>
          {transactionList.length > 0 ? (
                            <Table style={{marginTop:"5%"}}>
                            <thead>
                              <tr>
                                <th>Nazwa</th>
                                <th>Ilość</th>
                                <th>Aktualna cena</th>
                                <th>Operacja</th>
                              </tr>
                            </thead>
                            <tbody>
                            {transactionList.map((item,index) => {
                              if(item.type === "BUY"){
                                return(
                                  <tr key={index} style={{backgroundColor:"rgba(130, 255, 71, 0.5)"}}>
                                  <td>{item.symbol}</td>
                                  <td>{item.amount}</td>
                                  <td>{item.price}</td>
                                  <td>{item.type === "BUY" ? ("Kupno") : ("Sprzedaż")}</td>
                                  <td><Button onClick={()=>{deleteTransaction(index)}}  variant="danger">Usuń</Button></td>
                              </tr>
                                )
                              }
                              else{
                                return(
                                  <tr key={index} style={{backgroundColor:"rgba(255, 99, 71, 0.5)"}}>
                                  <td>{item.symbol}</td>
                                  <td>{item.amount}</td>
                                  <td>{item.price}</td>
                                  <td>{item.type === "BUY" ? ("Kupno") : ("Sprzedaż")}</td>
                                  <td><Button onClick={()=>{deleteTransaction(index)}}  variant="danger">Usuń</Button></td>
                              </tr>
                                )
                              }
                            } )}
                            </tbody>
                          </Table>
                        ): (<Alert variant="primary">Grupa jest pusta</Alert>)}
          <Form onSubmit={handleSubmit(onSubmitTransaction)}>
          <Form.Label>Rodzaj transakcji</Form.Label>
            <Form.Control as="select" required {...register("type")}>
              <option value="BUY">Kupno</option>
              <option value="SELL">Sprzedaż</option>
            </Form.Control>
            <Form.Group controlId="exampleForm.ControlInput1">
              <Form.Label>Nazwa kryptowaluty</Form.Label>
              <Form.Control  placeholder="Bitcoin" required {...register("name")}/>
            </Form.Group>
            <Form.Group controlId="exampleForm.ControlInput1">
              <Form.Label>Ilość zakupionej waluty</Form.Label>
              <Form.Control type="number" step="any"  placeholder="2.21" required {...register("amount")}/>
            </Form.Group>
            <Form.Group controlId="exampleForm.ControlInput1">
              <Form.Label>Aktualna cena waluty</Form.Label>
              <Form.Control type="number" step="any" placeholder="5.123" required {...register("price")}/>
            </Form.Group>
            <Button type="submit"  >
              Dodaj transakcję
            </Button>
            </Form>
          </Modal.Body>
          <Modal.Footer>
            <Button onClick={props.handleClose} variant="danger" >Zamknij</Button>
          </Modal.Footer>
     
      </Modal>
    )
}

export default GroupModal
