import React from 'react'
import styles from '../styles/styles.module.css'

export default function Footer(props) {
    return (
        <div className={styles.footer}>
            <p>{props.text}</p>
        </div>    
    )
}
