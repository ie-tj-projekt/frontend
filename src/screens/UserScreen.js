import React, {useState, useEffect} from 'react'
import {useDispatch, useSelector} from 'react-redux'
import { userSelector, loginUser } from '../store/userSlice'
import {Container, Row, Col} from 'react-bootstrap'
import TransactionGroup from '../components/TransactionGroup'
import TransactionsDetails from '../components/TransactionsDetails'
import UserBlock from '../components/UserBlock'
import {groupSelector, getGroup} from '../store/groupSlice'




export default function UserScreen({history}) {


    const [activeGroup, setActiveGroup] = useState()

    const dispatch = useDispatch()

    const { groups, firstName, lastName,isFetching, isSuccess, isError,isLogged, errorMessage } = useSelector(userSelector)
    //const {isGroupFetching, isGroupSuccess, isGroupError, groupErrorMessage} = useState(groupSelector)

    useEffect(() => {
        
        if(!isLogged){
          history.push('/login')
        }
        console.log(groups);
      }, [isLogged])

  
    
  
    

    return (
       <Container fluid style={{height:"85vh"}}>
         <Row noGutters={true}>
           <Col xs={2}>
              <UserBlock/>
           </Col>
           <Col xs={3}>
              <TransactionGroup   />
           </Col>
           <Col xs={7}>
              <TransactionsDetails   />
           </Col>
         </Row>
       </Container>
    )
}
