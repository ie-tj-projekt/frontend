import React, {useState, useEffect} from 'react'
import {useDispatch, useSelector} from 'react-redux'
import FormImpl from 'react-bootstrap/esm/Form'
import styles from '../styles/styles.module.css'
import {Table, Alert, Button} from 'react-bootstrap'
import TransactionModal from '../components/TransactionModal'
import { userSelector,deleteTransaction, deleteGroup } from '../store/userSlice'
import dayjs from 'dayjs'


function TransactionsDetails(props) {

    const { groups,activeGroup,isLogged,isFetching, errorMessage } = useSelector(userSelector)

    const dispatch = useDispatch()

    const [show, setShow] = useState(false);
    const [group, setGroup] = useState()
    const handleClose = () => setShow(false);
    const handleShow = () => setShow(true);

    useEffect(() => {
      if(isLogged){
        if(activeGroup != null){
            setGroup(activeGroup)
            console.log("GRUPKA" + activeGroup);
        }
      }
    }, [activeGroup,groups])

    const deleteTransactionHandler = (selectedIndex) => {
        console.log("delete handler");

        const transaction = group.transactions.find((item,index) => index === selectedIndex)

        console.log(transaction);
        console.log(activeGroup.name);

        const data = {
            name: activeGroup.name,
            date: transaction.date
        }

        dispatch(deleteTransaction(data))

      
    }

    const deleteGroupHandler = () => {


        const groupName = {name: activeGroup.name}
        console.log("Passed name: " + JSON.stringify(groupName));
        dispatch(deleteGroup(groupName))
    }

   

    return (
        <div className={styles.transactionDetailsContainer}>
        <TransactionModal
            handleClose={handleClose}
            show={show}
            group={group}
        />
        {isFetching ? (<h2>Loading</h2>) : (activeGroup != '') ? (
                    <div className={styles.transactionDetailsContent}>
                        <h2>Szczegóły</h2>
                        <div className={styles.detailsContainer}>
                        <div className={styles.groupDesc}>
                                <h4>Nazwa: {group.name}</h4>
                                <h6>Data założenia: {dayjs(group.creationDate).format('DD/MM/YYYY HH:mm:ss')}</h6>
                        </div>
                        {group.transactions ? (
                            <Table style={{marginTop:"5%", overflowY: "scroll"}}>
                            <thead>
                              <tr>
                                <th>Nazwa</th>
                                <th>Ilość</th>
                                <th>Cena</th>
                                <th>Wartość</th>
                                <th>Operacja</th>
                              </tr>
                            </thead>
                            <tbody>
                            {group.transactions.map((item,index) => {
                                if(item.type === "BUY"){
                                    return(
                                        <tr key={item.index} style={{backgroundColor:"rgba(130, 255, 71, 0.5)"}}>
                                    <td>{item.symbol}</td>
                                    <td>{item.amount}</td>
                                    <td>{item.price} $</td>
                                    <td>{Math.round(item.price * item.amount * 1000)/1000} $</td>
                                    <td>{item.type === "BUY" ? ("Kupno") : ("Sprzedaż")}</td>
                                    <td><Button onClick={()=>deleteTransactionHandler(index)}  variant="danger">Usuń</Button></td>
                                </tr>
                                    )
                                }
                                else{
                                    return(
                                        <tr key={item.index} style={{backgroundColor:"rgba(255, 99, 71, 0.5)"}}>
                                    <td>{item.symbol}</td>
                                    <td>{item.amount}</td>
                                    <td>{item.price} $</td>
                                    <td>{Math.round(item.price * item.amount * 1000)/1000} $</td>
                                    <td>{item.type === "BUY" ? ("Kupno") : ("Sprzedaż")}</td>
                                    <td><Button onClick={()=>deleteTransactionHandler(index)}  variant="danger">Usuń</Button></td>
                                </tr>
                                    )
                                }
                            } )}
                            </tbody>
                          </Table>
                        ): (<Alert  variant="primary">Grupa jest pusta</Alert>)}
                        </div>
                        <div>
                            <Button  onClick={handleShow}>Dodaj transkację</Button>
                            <Button variant="danger"  onClick={deleteGroupHandler}>Usuń grupę</Button>
                        </div>
                    </div>
                    
                )  :(<Alert style={{marginTop:"30%"}} variant="primary">Nie można załadować grupy</Alert>)}
        </div>
    )
}

export default TransactionsDetails
