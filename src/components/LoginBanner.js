import React from 'react'
import styles from '../styles/styles.module.css'
export default function LoginBanner(props) {

    return (
        <div className={styles.loginBanner}>
            <h1>{props.name}</h1>
        </div>
    )
}
