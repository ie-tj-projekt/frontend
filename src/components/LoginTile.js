import React, {useState, useEffect} from 'react'
import {useDispatch, useSelector} from 'react-redux'
import styles from '../styles/styles.module.css'
import {Alert, Button, Form} from 'react-bootstrap'
import { userSelector, loginUser } from '../store/userSlice'
import { useHistory } from 'react-router-dom';
import Loader from "react-loader-spinner";




export default function LoginTile() {

    const dispatch = useDispatch()
    const { isFetching, isSuccess, isError, errorMessage } = useSelector(userSelector)
    const history = useHistory();


    const [creds, setCreds] = useState({email:"", password:""})
    const [email, setEmail] = useState()
    const [password, setPassword] = useState()
    const [errorMsg, setErrorMsg] = useState()

    const submitHandler = e => {
        e.preventDefault();
        if(!email || !password){
            setErrorMsg('Musisz uzupelnić dane do logowania')
            return
        }

        const userCreds = {
            email: email,
            password: password
        }

        dispatch(loginUser({email,password}))
        
    }

    useEffect(() => {
       if(isError){
           setErrorMsg(errorMessage)
           console.log(errorMessage);
       }

       if(isSuccess){
           history.push('/')
       }
    }, [isError, isSuccess])

    



    return (
        <div className={styles.loginTile}>
            <h3 className={styles.userTileTitle}>Logowanie</h3>
            <Form 
                onSubmit={submitHandler}
                style={{
                    display:"flex",
                    flexDirection:"column",
                    justifyContent:"center",
                    alignItems:"center"
                }}>
                <Form.Group controlId="formBasicEmail">
                    <Form.Label>Email</Form.Label>
                    <Form.Control 
                        type="email" 
                        placeholder="Wprowadź adres email"
                        value={email}
                        onChange={(e)=>setEmail(e.target.value)} />
                </Form.Group>

                <Form.Group  controlId="formBasicPassword">
                    <Form.Label>Hasło</Form.Label>
                    <Form.Control 
                        type="password" 
                        placeholder="Wprowadź hasło"
                        value={password}
                        onChange={(e)=>setPassword(e.target.value)} />
                </Form.Group>
                <Button variant="success" type="submit"  style={{
                    width:"200px"
                }}>
                    Zaloguj
                </Button>
                {(isFetching) ? <Loader
                    type="Rings"
                    height={50}
                    width={50}
                    timeout={3000} //3 secs
                /> : null}
                {(errorMsg) ? <Alert style={{marginTop:"5%"}} variant="danger" >{errorMsg}</Alert> : null}
            </Form>

        </div>
    )
}
